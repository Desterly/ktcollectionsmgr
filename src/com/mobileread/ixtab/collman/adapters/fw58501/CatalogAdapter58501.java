package com.mobileread.ixtab.collman.adapters.fw58501;

import java.util.Date;

import com.amazon.ebook.util.a.B;
import com.amazon.ebook.util.text.d;
import com.amazon.kindle.content.catalog.f;
import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;
import com.amazon.kindle.content.catalog.CatalogItem;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;

public class CatalogAdapter58501 extends CatalogAdapter {

	public void setMembers(MutableCollection mutable, Object[] uuids) {
		B[] ids = asUUIDArray(uuids);
		mutable.MO(ids);
	}

	protected static B[] asUUIDArray(Object[] in) {
		if (in == null) {
			return null;
		}
		B[] out = new B[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (B) in[i];
		}
		return out;
	}

	public void addMember(MutableCollection mutable, Object uuid) {
		mutable.rO((B) uuid);
	}

	public void setIsVisibleInRoot(MutableCollection mutable,
			boolean visible) {
		setIsVisibleInHome(mutable, visible);
	}

	public void setTitle(MutableCollection mutable, String title) {
		setTitle((MutableEntry)mutable, title);
	}

	public Object getUUID(CatalogEntry entry) {
		return entry.ZL();
	}

	public String getLocation(CatalogEntry entry) {
		return entry.SJ();
	}

	public String getTitle(CatalogEntry entry) {
		d[] titles = entry.Xk();
		return (titles == null || titles.length < 1) ? "" : titles[0].toString();
	}

	public String getCDEKey(CatalogItem item) {
		return item.Kl();
	}

	public String getCDEType(CatalogItem item) {
		return item.wJ();
	}

	public Date getLastAccessDate(CatalogEntryCollection collection) {
		return collection.fk();
	}

	public boolean isVisibleInHome(CatalogEntry entry) {
		return entry.Rm();
	}

	public void setIsVisibleInHome(MutableEntry mutable, boolean visible) {
		mutable.nM(visible);
	}

	public boolean hasCloudCollections() {
		return true;
	}

	public int countParents(CatalogEntry entry) {
		return getParents(entry).length;
	}

	public Object[] getParents(CatalogEntry entry) {
		return entry.VK();
	}

	public String getFirstCreditAsJSON(CatalogEntry entry) {
		f[] credits = entry.dk();
		if (credits == null || credits.length < 1) {
			return null;
		}
		return credits[0].toJSONString();
	}

	public Object[] getMembers(CatalogEntryCollection collection) {
		return collection.Fo();
	}

	public int countMembers(CatalogEntryCollection collection) {
		return collection.Po();
	}

	public void setTitle(MutableEntry mutable, String title) {
		mutable.uM(new d[] {new d(title)});
	}

}
