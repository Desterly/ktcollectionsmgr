package com.mobileread.ixtab.collman.adapters.fw581;

import com.amazon.ebook.util.a.B;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;

public class CatalogTransaction581 extends CatalogTransaction {

	private final com.amazon.kindle.content.catalog.CatalogTransaction delegate;
	public CatalogTransaction581(
			com.amazon.kindle.content.catalog.CatalogTransaction delegate) {
		this.delegate = delegate;
	}

	public void deleteEntry(Object uuid) {
		delegate.fN((B) uuid);
	}

	public boolean commitSync() {
		return delegate.en().WUC();
	}

	public void addEntry(MutableEntry c) {
		delegate.UO(c);
	}

	public void updateEntry(MutableEntry entry) {
		delegate.uM(entry);
	}

}
