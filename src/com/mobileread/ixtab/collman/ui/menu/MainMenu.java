package com.mobileread.ixtab.collman.ui.menu;

import com.amazon.kindle.kindlet.ui.KMenuItem;
import com.mobileread.ixtab.collman.I18n;
import com.mobileread.ixtab.collman.ui.menu.display.DisplayOptionsMenu;
import com.mobileread.ixtab.collman.ui.menu.sort.SortOptionsMenu;

public class MainMenu extends VirtualMenu {
	
	public MainMenu() {
		super(null);
	}

	public String getLabel() {
		return I18n.get().i18n(I18n.MENU_MAIN_KEY, I18n.MENU_MAIN_VALUE);
	}

	protected KMenuItem[] getItems() {
		
		VirtualMenu[] sortMenus = SortOptionsMenu.getCurrentInstances(this);
		KMenuItem[] items = new KMenuItem[2 + sortMenus.length];
		int i=0;
		items[i++] = wrap(new DisplayOptionsMenu(this));
		for (int j=0; j < sortMenus.length; ++j) {
			items[i++] = wrap(sortMenus[j]);
		}
		items[i++] = wrap(new SyncMenu(this));
		return items;
	}

}
