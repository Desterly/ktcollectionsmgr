package com.mobileread.ixtab.collman.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.I18n;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.Settings;

public class ToggleShowInvisibleItemsAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	
	public ToggleShowInvisibleItemsAction() {
		super(I18n.get().i18n(I18n.INVISIBLE_SHOW_KEY, I18n.INVISIBLE_SHOW_VALUE));
	}

	public void actionPerformed(ActionEvent e) {
		Event.post(Settings.get().isHideInvisibleInHome() ? Event.INVISIBLE_SHOW : Event.INVISIBLE_HIDE, this, PanelPosition.BOTH);
	}
	
}
