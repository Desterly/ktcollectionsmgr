package com.mobileread.ixtab.collman.adapters.fw585;

import com.amazon.ebook.util.a.B;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;

public class CatalogTransaction585 extends CatalogTransaction {

	private final com.amazon.kindle.content.catalog.CatalogTransaction delegate;
	public CatalogTransaction585(
			com.amazon.kindle.content.catalog.CatalogTransaction delegate) {
		this.delegate = delegate;
	}

	public void deleteEntry(Object uuid) {
		delegate.qN((B) uuid);
	}

	public boolean commitSync() {
		return delegate.FM().vWC();
	}

	public void addEntry(MutableEntry c) {
		delegate.SO(c);
	}

	public void updateEntry(MutableEntry entry) {
		delegate.Sp(entry);
	}

}
