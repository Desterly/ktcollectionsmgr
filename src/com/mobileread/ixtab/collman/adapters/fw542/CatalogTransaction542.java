package com.mobileread.ixtab.collman.adapters.fw542;

import com.amazon.ebook.util.a.h;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;

public class CatalogTransaction542 extends CatalogTransaction {

	private final com.amazon.kindle.content.catalog.CatalogTransaction delegate;
	public CatalogTransaction542(
			com.amazon.kindle.content.catalog.CatalogTransaction delegate) {
		this.delegate = delegate;
	}

	public void deleteEntry(Object uuid) {
		delegate.Yk((h) uuid);
	}

	public boolean commitSync() {
		return delegate.QJ().JEc();
	}

	public void addEntry(MutableEntry c) {
		delegate.Dl(c);
	}

	public void updateEntry(MutableEntry entry) {
		delegate.UL(entry);
	}

}
