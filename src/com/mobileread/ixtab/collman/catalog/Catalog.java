package com.mobileread.ixtab.collman.catalog;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactory;
import com.mobileread.ixtab.collman.adapters.CatalogService.QueryResultDepth;

public class Catalog {

	public static class Predicates {
		public static final Predicate ALL_ENTRIES = PredicateFactory.startsWith(
				"titles[0].display", "");
		
		private static Predicate and(Predicate p1, Predicate p2) {
			return PredicateFactory.and(new Predicate[] {p1,p2});
		}
		
		private static Predicate or(Predicate p1, Predicate p2) {
			return PredicateFactory.or(new Predicate[] {p1,p2});
		}
		
		private static Predicate not(Predicate p1) {
			return PredicateFactory.not(p1);
		}
		
		private static Predicate notDownloading = not(PredicateFactory.isTrue("isDownloading"));
		private static Predicate notAcx = not(PredicateFactory.equals("mimeType", "application/x-kindle-acx"));
		private static Predicate notArchived = not(PredicateFactory.isTrue("isArchived"));

		private static Predicate item = PredicateFactory.startsWith("type", "Entry:Item");
		private static Predicate collection = PredicateFactory.startsWith("type", "Collection");
		
		
		private static Predicate sane = and(notDownloading, and(notAcx, notArchived));
		
		private static Predicate saneItem = and(item, sane);
		private static Predicate saneCollection = and(collection, sane);
		private static Predicate standardFilter = or(saneItem, saneCollection);
	}
	

	private static SearchCache cache;
	
	private static final CatalogService backend = CatalogService.INSTANCE;

	private Catalog() {
	};

	public static void init(boolean isSearchSupported) {
		cache = createCache(isSearchSupported);
	}

	private static SearchCache createCache(boolean isSearchSupported) {
		if (isSearchSupported) {
			final WorkingSearchCache cache = new WorkingSearchCache();
			new Thread() {

				public void run() {
					cache.init(find(PanelPosition.BOTH, Predicates.ALL_ENTRIES, true, 0, Integer.MAX_VALUE,
							null, QueryResultDepth.FAST));
				}

			}.start();
			return cache;
		} else {
			return new NoOpSearchCache();
		}
	}

	public static SearchCache getCache() {
		return cache;
	}

	public static CatalogEntry[] find(Predicate predicate) {
		CatalogEntry[] result = find(PanelPosition.BOTH, predicate, true, 0, Integer.MAX_VALUE,
				null);
		return result;
	}
	
	public static CatalogEntry[] find(Predicate predicate,
			boolean useStandardFilter, final int offset, final int maxResults,
			final int[] totalCountPointer) {
		return find(PanelPosition.BOTH, predicate, useStandardFilter, offset, maxResults,
				totalCountPointer, QueryResultDepth.FULL);
	}
	
	public static CatalogEntry[] find(int panel, Predicate predicate,
			boolean useStandardFilter, final int offset, final int maxResults,
			final int[] totalCountPointer) {
		return find(panel, predicate, useStandardFilter, offset, maxResults,
				totalCountPointer, QueryResultDepth.FULL);
	}

	public static CatalogEntry[] find(Predicate predicate,
			boolean useStandardFilter, final int offset, final int maxResults,
			final int[] totalCountPointer, final QueryResultDepth depth) {
		return find(PanelPosition.BOTH, predicate, useStandardFilter, offset, maxResults, totalCountPointer, depth);
	}
	
	public static CatalogEntry[] find(int panel, Predicate predicate,
			boolean useStandardFilter, final int offset, final int maxResults,
			final int[] totalCountPointer, final QueryResultDepth depth) {
		if (useStandardFilter) {
			predicate = Predicates.and(predicate, Predicates.standardFilter);
		}
		return backend.find(panel, predicate, offset, maxResults, totalCountPointer,
				depth);
	}

	public static MutableEntry createMutableEntry(Entry entry) {
		return backend.createMutableEntry(entry.getUuid());
	}

	public static MutableCollection createMutableCollection(
			Collection collection) {
		return backend.createMutableCollection(collection.getUuid());
	}

	public static void update(MutableEntry entry) {
		CatalogTransaction t = backend.openTransaction();
		t.updateEntry(entry);
		t.commitSync();
	}

	/* this method may return null if the catalog is wonky */
	public static CatalogEntry load(Object uuid) {
		if (uuid == null) return null;
		CatalogEntry[] results = find(getPredicateForUUID(uuid));
		if (results == null || results.length == 0) {
			return null;
		}
		return results[0];
	}

	private static Predicate getPredicateForUUID(Object uuid) {
		return PredicateFactory.equals("uuid", uuid.toString());
	}

	public static void delete(Object uuid) {
		CatalogTransaction t = backend.openTransaction();
		t.deleteEntry(uuid);
		t.commitSync();
	}

	public static Collection createCollection(Collection parent,
			String newCollectionTitle) {
		Object uuid = createRandomUUID();
		MutableCollection c = backend.createMutableCollection(uuid);
		CatalogAdapter.INSTANCE.setTitle(c, newCollectionTitle);
		CatalogAdapter.INSTANCE.setIsVisibleInRoot(c, parent.isRoot());
		CatalogTransaction t = backend.openTransaction();
		t.addEntry(c);
		boolean ok = t.commitSync();
		if (ok) {
			parent.addEntry(Entry.instantiate(load(uuid)));
			getCache().set(uuid, newCollectionTitle);
		}
		return (Collection) Entry.instantiate(load(uuid));
	}

	public static CatalogService getBackend() {
		return backend;
	}

	public static Object createRandomUUID() {
		return backend.createNewUUID();
	}

}
