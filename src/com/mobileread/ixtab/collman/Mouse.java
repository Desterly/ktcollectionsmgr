package com.mobileread.ixtab.collman;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Stack;

import com.mobileread.ixtab.collman.ui.MainPanel;

public class Mouse {
	
	public static class State {
		private static final int IGNORE_BELOW_MS = 50;
		private static final int SWIPE_THRESHOLD_PX = 100;
		private static final int CLICK_THRESHOLD_PX = 20;
		private static final int LONG_CLICK_THRESHOLD_MS = 1000;

		public final MouseEvent firstPressed;
		public final MouseEvent secondPressed;
		public final MouseEvent released;
		
		private State(MouseEvent firstPressed, MouseEvent secondPressed, MouseEvent released) {
			this.firstPressed = firstPressed;
			this.secondPressed = secondPressed;
			this.released = released;
		}
		
		private boolean isValid() {
			return firstPressed != null;
		}
		
		private boolean isRelease() {
			return isValid() && released != null;
		}
		
		private State transition(MouseEvent event, boolean isRelease) {
			if (!isValid()) {
				return new State(event, null, null);
			}
			if (isRelease) {
				MouseEvent lp = secondPressed != null ? secondPressed : firstPressed;
				if (lp != null && event.getWhen() - lp.getWhen() < IGNORE_BELOW_MS) {
					return this;
				}
				return new State(firstPressed, secondPressed, event);
			} else {
				if (released != null) {
					return new State(event, null, null);
				} else {
					return new State(firstPressed, event, null);
				}
			}
		}
		
		public boolean isClick() {
			if (isRelease()) {
				return Math.abs(getXDelta()) < CLICK_THRESHOLD_PX && Math.abs(getYDelta()) < CLICK_THRESHOLD_PX;
			}
			return false;
		}
		
		public boolean isLongClick() {
			return isClick() && getTimeDelta() >= LONG_CLICK_THRESHOLD_MS;
		}
		
		public long getTimeDelta() {
			if (!isRelease()) return 0;
			return released.getWhen() - firstPressed.getWhen();
		}
		
		public long getXDelta() {
			if (!isRelease()) return 0;
			return released.getX() - firstPressed.getX();
		}
		
		public long getYDelta() {
			if (!isRelease()) return 0;
			return released.getY() - firstPressed.getY();
		}
		
		public boolean isSwipeToEast() {
			return getXDelta() >= SWIPE_THRESHOLD_PX && Math.abs(getYDelta()) < SWIPE_THRESHOLD_PX;
		}
		
		public boolean isSwipeToWest() {
			return getXDelta() <= -SWIPE_THRESHOLD_PX && Math.abs(getYDelta()) < SWIPE_THRESHOLD_PX;
		}
		
		public boolean isSwipeToNorth() {
			return getYDelta() <= -SWIPE_THRESHOLD_PX && Math.abs(getXDelta()) < SWIPE_THRESHOLD_PX;
		}
		
		public boolean isSwipeToSouth() {
			return getYDelta() >= SWIPE_THRESHOLD_PX && Math.abs(getXDelta()) < SWIPE_THRESHOLD_PX;
		}
		
		public String toString() {
			StringBuffer b = new StringBuffer();
			b.append(firstPressed == null ? " null ": (firstPressed.getX() + "," + firstPressed.getY()));
			b.append(" ");
			b.append(secondPressed == null ? " null ": (secondPressed.getX() + "," + secondPressed.getY()));
			b.append(" ");
			b.append(released == null ? " null ": (released.getX() + "," + released.getY()));
			return b.toString();
		}
	}
	
	private static State currentState = new State(null, null, null);

	public static void initialize(final MainPanel main) {
		MouseAdapter adapter = new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				currentState = currentState.transition(e, false);
			}

			public void mouseReleased(MouseEvent e) {
				State oldState = currentState;
				currentState = currentState.transition(e, true);
				if (oldState != currentState) {
					bubbleEventDown(currentState);
				}
			}

			private void bubbleEventDown(State state) {
				Stack stack = getStackFor(state);
				while (!stack.isEmpty()) {
					Sensitive handler = (Sensitive) stack.pop();
					if (handler.handleMouseReleased(state)) {
						break;
					}
				}
			}
			
			private Stack getStackFor(State state) {
				Stack stack = new Stack();
				Component c = main.findComponentAt(state.released.getPoint());
				while (true) {
					if (c instanceof Sensitive) {
						stack.add(c);
					}
					if (c == main) {
						break;
					}
					c = c.getParent();
				}
				return stack;
			}
		};
		main.addMouseListener(adapter);
	}
	
	public interface Sensitive {
		public boolean handleMouseReleased(State state);
	}
}
