package com.mobileread.ixtab.collman.adapters.fw58501;

import java.lang.reflect.Field;

import com.amazon.kindle.booklet.BookletContext;
import com.amazon.kindle.booklet.c;
import com.amazon.kindle.booklet.l;
import com.amazon.kindle.booklet.g;
import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.fw58501.SearchHandler58501;

public class CollectionManager58501 extends CollectionsManager {

	private static final long serialVersionUID = 1L;

	public CollectionManager58501(KindletContext context) {
		super(context);
	}

	protected BookletContext getBookletContext() {
		try {
			com.amazon.kindle.kindlet.internal.ui.E x = (com.amazon.kindle.kindlet.internal.ui.E) context.getService(com.amazon.kindle.kindlet.ui.Toolbar.class);
			Field f = x.getClass().getDeclaredField("M"); //ChromeHeaderRequest??
			f.setAccessible(true);

			f = x.getClass().getDeclaredField("g"); //BookletContext??
			f.setAccessible(true);
			return (BookletContext) f.get(x);
		} catch (Throwable t) {
			return null;
		}
	}

	protected void modifyToolbar() {
		if (chromeImplementation == null || bookletContext == null) {
			return;
		}

		boolean registerSearch = true;
		try {
			chromeImplementation.lgB(bookletContext, "default", SearchHandler58501
					.getInstance58501().getChromeSearchProvider());
		} catch (c e) {
			registerSearch = false;
		}
		if (registerSearch) {
			SearchHandler58501.getInstance58501().register(bookletContext);
		}

		try {
			g sbr = new g(
					"com.lab126.booklet.kindlet");
			sbr.GXC("default",
					new l[] {
							new l("forward", "invisible",
									"system"),
							new l("back", "invisible",
									"system"), });
			chromeImplementation.DL(bookletContext, sbr, true);
		} catch (c e) {
			// this is actually known to throw an exception, yet it still
			// provides the wanted result. Go figure.
		}
		}

	public void onStop() {
		SearchHandler58501.getInstance58501().unregister(bookletContext);
	}
	
	protected boolean isSearchSupported() {
		return true;
	}

}
