package com.mobileread.ixtab.collman.adapters.fw581;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;
import com.mobileread.ixtab.collman.adapters.SearchHandler;

public class AdapterConfiguration581 extends AdapterConfiguration {

	public AdapterConfiguration581() {

	};

	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter581();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter581();
	}

	public CatalogService getCatalogService() {
		return new CatalogService581();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager581(context);
	}

	public SearchHandler getSearchHandler() {
		return new SearchHandler581();
	}

}
