package com.mobileread.ixtab.collman.adapters.fw543;

import com.amazon.ebook.util.a.h;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;

public class CatalogTransaction543 extends CatalogTransaction {

	private final com.amazon.kindle.content.catalog.CatalogTransaction delegate;
	public CatalogTransaction543(
			com.amazon.kindle.content.catalog.CatalogTransaction delegate) {
		this.delegate = delegate;
	}

	public void deleteEntry(Object uuid) {
		delegate.dm((h) uuid);
	}

	public boolean commitSync() {
		return delegate.Gl().uGc();
	}

	public void addEntry(MutableEntry c) {
		delegate.vJ(c);
	}

	public void updateEntry(MutableEntry entry) {
		delegate.Ok(entry);
	}
}
