package com.mobileread.ixtab.collman.adapters.fw587;

import com.amazon.ebook.util.a.B;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;

public class CatalogTransaction587 extends CatalogTransaction {

	private final com.amazon.kindle.content.catalog.CatalogTransaction delegate;
	public CatalogTransaction587(
			com.amazon.kindle.content.catalog.CatalogTransaction delegate) {
		this.delegate = delegate;
	}

	public void deleteEntry(Object uuid) {
		delegate.Sp((B) uuid);
	}

	public boolean commitSync() {
		return delegate.Op().LYC();
	}

	public void addEntry(MutableEntry c) {
		delegate.iN(c);
	}

	public void updateEntry(MutableEntry entry) {
		delegate.Lo(entry);
	}

}
