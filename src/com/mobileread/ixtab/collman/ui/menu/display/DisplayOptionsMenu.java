package com.mobileread.ixtab.collman.ui.menu.display;

import com.amazon.kindle.kindlet.ui.KMenuItem;
import com.mobileread.ixtab.collman.I18n;
import com.mobileread.ixtab.collman.ui.menu.VirtualMenu;

public class DisplayOptionsMenu extends VirtualMenu {

	private final KMenuItem[] items;
	public DisplayOptionsMenu(VirtualMenu parent) {
		super(parent);
		items = new KMenuItem[5];
		int i=0;
		items[i++] = getParentItem();
		items[i++] = new KMenuItem(new ShowInvisibleInHomeButton());
		items[i++] = new KMenuItem(new ShowCollectionsOnlyButton());
		items[i++] = new KMenuItem(new FilterLowerPanelOnlyButton());
		items[i++] = new KMenuItem(new ShowUnfiledItemsOnlyButton());
	}

	public String getLabel() {
		return I18n.get().i18n(I18n.MENU_DISPLAYOPTIONS_KEY, I18n.MENU_DISPLAYOPTIONS_VALUE);
	}

	protected KMenuItem[] getItems() {
		return items;
	}

}
