package com.mobileread.ixtab.collman.adapters.fw585;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;
import com.mobileread.ixtab.collman.adapters.SearchHandler;

public class AdapterConfiguration585 extends AdapterConfiguration {

	public AdapterConfiguration585() {

	};

	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter585();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter585();
	}

	public CatalogService getCatalogService() {
		return new CatalogService585();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager585(context);
	}

	public SearchHandler getSearchHandler() {
		return new SearchHandler585();
	}

}
