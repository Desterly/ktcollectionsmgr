package com.mobileread.ixtab.collman.adapters.fw585;

import java.util.Date;

import com.amazon.ebook.util.a.B;
import com.amazon.ebook.util.text.d;
import com.amazon.kindle.content.catalog.f;
import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;
import com.amazon.kindle.content.catalog.CatalogItem;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;

public class CatalogAdapter585 extends CatalogAdapter {

	public void setMembers(MutableCollection mutable, Object[] uuids) {
		B[] ids = asUUIDArray(uuids);
		mutable.Ho(ids);
	}

	protected static B[] asUUIDArray(Object[] in) {
		if (in == null) {
			return null;
		}
		B[] out = new B[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (B) in[i];
		}
		return out;
	}

	public void addMember(MutableCollection mutable, Object uuid) {
		mutable.LM((B) uuid);
	}

	public void setIsVisibleInRoot(MutableCollection mutable,
			boolean visible) {
		setIsVisibleInHome(mutable, visible);
	}

	public void setTitle(MutableCollection mutable, String title) {
		setTitle((MutableEntry)mutable, title);
	}

	public Object getUUID(CatalogEntry entry) {
		return entry.wm();
	}

	public String getLocation(CatalogEntry entry) {
		return entry.Ek();
	}

	public String getTitle(CatalogEntry entry) {
		d[] titles = entry.ik();
		return (titles == null || titles.length < 1) ? "" : titles[0].toString();
	}

	public String getCDEKey(CatalogItem item) {
		return item.wJ();
	}

	public String getCDEType(CatalogItem item) {
		return item.zl();
	}

	public Date getLastAccessDate(CatalogEntryCollection collection) {
		return collection.SJ();
	}

	public boolean isVisibleInHome(CatalogEntry entry) {
		return entry.Wl();
	}

	public void setIsVisibleInHome(MutableEntry mutable, boolean visible) {
		mutable.TO(visible);
	}

	public boolean hasCloudCollections() {
		return true;
	}

	public int countParents(CatalogEntry entry) {
		return getParents(entry).length;
	}

	public Object[] getParents(CatalogEntry entry) {
		return entry.Sk();
	}

	public String getFirstCreditAsJSON(CatalogEntry entry) {
		f[] credits = entry.Ak();
		if (credits == null || credits.length < 1) {
			return null;
		}
		return credits[0].toJSONString();
	}

	public Object[] getMembers(CatalogEntryCollection collection) {
		return collection.kM();
	}

	public int countMembers(CatalogEntryCollection collection) {
		return collection.xn();
	}

	public void setTitle(MutableEntry mutable, String title) {
		mutable.UO(new d[] {new d(title)});
	}

}
