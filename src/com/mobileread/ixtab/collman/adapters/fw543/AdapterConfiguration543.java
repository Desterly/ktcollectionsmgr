package com.mobileread.ixtab.collman.adapters.fw543;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;
import com.mobileread.ixtab.collman.adapters.SearchHandler;

public class AdapterConfiguration543 extends AdapterConfiguration {

	public AdapterConfiguration543() {

	};

	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter543();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter543();
	}

	public CatalogService getCatalogService() {
		return new CatalogService543();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager543(context);
	}

	public SearchHandler getSearchHandler() {
		return new SearchHandler543();
	}

}
