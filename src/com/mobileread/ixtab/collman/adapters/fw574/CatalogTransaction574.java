package com.mobileread.ixtab.collman.adapters.fw574;

import com.amazon.ebook.util.a.B;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;

public class CatalogTransaction574 extends CatalogTransaction {

	private final com.amazon.kindle.content.catalog.CatalogTransaction delegate;
	public CatalogTransaction574(
			com.amazon.kindle.content.catalog.CatalogTransaction delegate) {
		this.delegate = delegate;
	}

	public void deleteEntry(Object uuid) {
		delegate.Cp((B) uuid);
	}

	public boolean commitSync() {
		return delegate.ln().YUC();
	}

	public void addEntry(MutableEntry c) {
		delegate.rp(c);
	}

	public void updateEntry(MutableEntry entry) {
		delegate.fN(entry);
	}

}
