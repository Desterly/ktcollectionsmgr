package com.mobileread.ixtab.collman.adapters.fw58501;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;
import com.mobileread.ixtab.collman.adapters.SearchHandler;

public class AdapterConfiguration58501 extends AdapterConfiguration {

	public AdapterConfiguration58501() {

	};

	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter58501();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter58501();
	}

	public CatalogService getCatalogService() {
		return new CatalogService58501();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager58501(context);
	}

	public SearchHandler getSearchHandler() {
		return new SearchHandler58501();
	}

}
