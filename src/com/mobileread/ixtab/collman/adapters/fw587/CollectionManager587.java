package com.mobileread.ixtab.collman.adapters.fw587;

import java.lang.reflect.Field;

import com.amazon.kindle.booklet.BookletContext;
import com.amazon.kindle.booklet.c;
import com.amazon.kindle.booklet.l;
import com.amazon.kindle.booklet.g;
import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.fw587.SearchHandler587;

public class CollectionManager587 extends CollectionsManager {

	private static final long serialVersionUID = 1L;

	public CollectionManager587(KindletContext context) {
		super(context);
	}

	protected BookletContext getBookletContext() {
		try {
			com.amazon.kindle.kindlet.internal.ui.E x = (com.amazon.kindle.kindlet.internal.ui.E) context.getService(com.amazon.kindle.kindlet.ui.Toolbar.class);
			Field f = x.getClass().getDeclaredField("M"); //ChromeHeaderRequest??
			f.setAccessible(true);

			f = x.getClass().getDeclaredField("g"); //BookletContext??
			f.setAccessible(true);
			return (BookletContext) f.get(x);
		} catch (Throwable t) {
			return null;
		}
	}

	protected void modifyToolbar() {
		if (chromeImplementation == null || bookletContext == null) {
			return;
		}

		boolean registerSearch = true;
		try {
			chromeImplementation.UFB(bookletContext, "default", SearchHandler587
					.getInstance587().getChromeSearchProvider());
		} catch (c e) {
			registerSearch = false;
		}
		if (registerSearch) {
			SearchHandler587.getInstance587().register(bookletContext);
		}

		try {
			g sbr = new g(
					"com.lab126.booklet.kindlet");
			sbr.zZC("default",
					new l[] {
							new l("forward", "invisible",
									"system"),
							new l("back", "invisible",
									"system"), });
			chromeImplementation.OK(bookletContext, sbr, true);
		} catch (c e) {
			// this is actually known to throw an exception, yet it still
			// provides the wanted result. Go figure.
		}
		}

	public void onStop() {
		SearchHandler587.getInstance587().unregister(bookletContext);
	}
	
	protected boolean isSearchSupported() {
		return true;
	}

}
