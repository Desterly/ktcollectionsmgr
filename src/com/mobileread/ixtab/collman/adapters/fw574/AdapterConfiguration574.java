package com.mobileread.ixtab.collman.adapters.fw574;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;
import com.mobileread.ixtab.collman.adapters.SearchHandler;

public class AdapterConfiguration574 extends AdapterConfiguration {

	public AdapterConfiguration574() {

	};

	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter574();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter574();
	}

	public CatalogService getCatalogService() {
		return new CatalogService574();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager574(context);
	}

	public SearchHandler getSearchHandler() {
		return new SearchHandler574();
	}

}
