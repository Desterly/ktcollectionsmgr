package com.mobileread.ixtab.collman.adapters.fw581;

import java.lang.reflect.Field;

import com.amazon.kindle.booklet.BookletContext;
import com.amazon.kindle.booklet.G;
import com.amazon.kindle.booklet.l;
import com.amazon.kindle.booklet.g;
import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.fw581.SearchHandler581;

public class CollectionManager581 extends CollectionsManager {

	private static final long serialVersionUID = 1L;

	public CollectionManager581(KindletContext context) {
		super(context);
	}

	protected BookletContext getBookletContext() {
		try {
			com.amazon.kindle.kindlet.internal.ui.E x = (com.amazon.kindle.kindlet.internal.ui.E) context.getService(com.amazon.kindle.kindlet.ui.Toolbar.class);
			Field f = x.getClass().getDeclaredField("M"); //ChromeHeaderRequest??
			f.setAccessible(true);

			f = x.getClass().getDeclaredField("g"); //BookletContext??
			f.setAccessible(true);
			return (BookletContext) f.get(x);
		} catch (Throwable t) {
			return null;
		}
	}

	protected void modifyToolbar() {
		if (chromeImplementation == null || bookletContext == null) {
			return;
		}

		boolean registerSearch = true;
		try {
			chromeImplementation.tEB(bookletContext, "default", SearchHandler581
					.getInstance581().getChromeSearchProvider());
		} catch (G e) {
			registerSearch = false;
		}
		if (registerSearch) {
			SearchHandler581.getInstance581().register(bookletContext);
		}

		try {
			g sbr = new g(
					"com.lab126.booklet.kindlet");
			sbr.xZC("default",
					new l[] {
							new l("forward", "invisible",
									"system"),
							new l("back", "invisible",
									"system"), });
			chromeImplementation.Cl(bookletContext, sbr, true);
		} catch (G e) {
			// this is actually known to throw an exception, yet it still
			// provides the wanted result. Go figure.
		}
		}

	public void onStop() {
		SearchHandler581.getInstance581().unregister(bookletContext);
	}
	
	protected boolean isSearchSupported() {
		return true;
	}

}
