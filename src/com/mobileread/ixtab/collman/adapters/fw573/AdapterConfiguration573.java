package com.mobileread.ixtab.collman.adapters.fw573;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;
import com.mobileread.ixtab.collman.adapters.SearchHandler;

public class AdapterConfiguration573 extends AdapterConfiguration {

	public AdapterConfiguration573() {

	};

	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter573();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter573();
	}

	public CatalogService getCatalogService() {
		return new CatalogService573();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager573(context);
	}

	public SearchHandler getSearchHandler() {
		return new SearchHandler573();
	}

}
