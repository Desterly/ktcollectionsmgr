package com.mobileread.ixtab.collman.adapters.fw574;

import java.util.Date;

import com.amazon.ebook.util.a.B;
import com.amazon.ebook.util.text.d;
import com.amazon.kindle.content.catalog.f;
import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;
import com.amazon.kindle.content.catalog.CatalogItem;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;

public class CatalogAdapter574 extends CatalogAdapter {

	public void setMembers(MutableCollection mutable, Object[] uuids) {
		B[] ids = asUUIDArray(uuids);
		mutable.gO(ids);
	}

	protected static B[] asUUIDArray(Object[] in) {
		if (in == null) {
			return null;
		}
		B[] out = new B[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (B) in[i];
		}
		return out;
	}

	public void addMember(MutableCollection mutable, Object uuid) {
		mutable.on((B) uuid);
	}

	public void setIsVisibleInRoot(MutableCollection mutable,
			boolean visible) {
		setIsVisibleInHome(mutable, visible);
	}

	public void setTitle(MutableCollection mutable, String title) {
		setTitle((MutableEntry)mutable, title);
	}

	public Object getUUID(CatalogEntry entry) {
		return entry.Ok();
	}

	public String getLocation(CatalogEntry entry) {
		return entry.fl();
	}

	public String getTitle(CatalogEntry entry) {
		d[] titles = entry.zk();
		return (titles == null || titles.length < 1) ? "" : titles[0].toString();
	}

	public String getCDEKey(CatalogItem item) {
		return item.fk();
	}

	public String getCDEType(CatalogItem item) {
		return item.SJ();
	}

	public Date getLastAccessDate(CatalogEntryCollection collection) {
		return collection.Gk();
	}

	public boolean isVisibleInHome(CatalogEntry entry) {
		return entry.Pm();
	}

	public void setIsVisibleInHome(MutableEntry mutable, boolean visible) {
		mutable.DO(visible);
	}

	public boolean hasCloudCollections() {
		return true;
	}

	public int countParents(CatalogEntry entry) {
		return getParents(entry).length;
	}

	public Object[] getParents(CatalogEntry entry) {
		return entry.QJ();
	}

	public String getFirstCreditAsJSON(CatalogEntry entry) {
		f[] credits = entry.vJ();
		if (credits == null || credits.length < 1) {
			return null;
		}
		return credits[0].toJSONString();
	}

	public Object[] getMembers(CatalogEntryCollection collection) {
		return collection.wn();
	}

	public int countMembers(CatalogEntryCollection collection) {
		return collection.Jn();
	}

	public void setTitle(MutableEntry mutable, String title) {
		mutable.jO(new d[] {new d(title)});
	}

}
