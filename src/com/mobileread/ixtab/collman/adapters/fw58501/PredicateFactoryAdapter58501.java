package com.mobileread.ixtab.collman.adapters.fw58501;

import com.amazon.kindle.content.catalog.B;
import com.amazon.kindle.content.catalog.L;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class PredicateFactoryAdapter58501 implements PredicateFactoryAdapter {

	private Predicate wrap(Object wrapped) {
		return new Predicate(wrapped);
	}

	private L[] explode(Predicate[] in) {
		if (in == null) {
			return null;
		}
		L[] out = new L[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (L) in[i].delegate;
		}
		return out;
	}

	public Predicate and(Predicate[] predicates) {
		return wrap(B.VUC(explode(predicates)));
	}

	public Predicate or(Predicate[] predicates) {
		return wrap(B.axC(explode(predicates)));
	}

	public Predicate not(Predicate pred) {
		return wrap(B.mWC((L) pred.delegate));
	}

	public Predicate notNull(String key) {
		return wrap(B.EWC(key));
	}

	public Predicate isTrue(String what) {
		return wrap(B.owC(what));
	}

	public Predicate equals(String key, String value) {
		return wrap(B.nnB(key, value));
	}

	public Predicate inList(String key, Object membersArray) {
		return wrap(B.iVC(key, CatalogAdapter58501
                .asUUIDArray((Object[]) membersArray)));
	}

	public Predicate greater(String key, long value, boolean inclusive) {
		return wrap(B.bwC(key, value, inclusive));
	}

	public Predicate startsWith(String key, String value) {
		return wrap(B.nwC(key, value));
	}

}
