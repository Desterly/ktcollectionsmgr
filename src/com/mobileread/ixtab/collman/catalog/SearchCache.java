package com.mobileread.ixtab.collman.catalog;


public interface SearchCache {

	public abstract void delete(Object uuid);

	public abstract void set(Object uuid, String newTitle);

	public abstract Object[] find(String search);

}