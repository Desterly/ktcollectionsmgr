package com.mobileread.ixtab.collman.icons;

import javax.swing.ImageIcon;

public class Icons {
	public static final String MOVE_UP = "mvup.png";
	public static final String MOVE_DOWN = "mvdn.png";
	public static final String COPY_UP = "cpup.png";
	public static final String COPY_DOWN = "cpdn.png";
	
	public static final String SHOW = "show.png";
	public static final String HIDE = "hide.png";
	public static final String GHOST = "hidden.png";
	public static final String TRASH = "trash.png";
	public static final String RENAME = "rename.png";
	public static final String CREATE = "create.png";
	
	public static ImageIcon load(String name) {
		return new ImageIcon(Icons.class.getResource(name));
	}
}
