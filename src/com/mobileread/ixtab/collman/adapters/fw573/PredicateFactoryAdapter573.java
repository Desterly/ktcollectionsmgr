package com.mobileread.ixtab.collman.adapters.fw573;

import com.amazon.kindle.content.catalog.B;
import com.amazon.kindle.content.catalog.L;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class PredicateFactoryAdapter573 implements PredicateFactoryAdapter {

	private Predicate wrap(Object wrapped) {
		return new Predicate(wrapped);
	}

	private L[] explode(Predicate[] in) {
		if (in == null) {
			return null;
		}
		L[] out = new L[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (L) in[i].delegate;
		}
		return out;
	}

	public Predicate and(Predicate[] predicates) {
		return wrap(B.gxC(explode(predicates)));
	}

	public Predicate or(Predicate[] predicates) {
		return wrap(B.uvC(explode(predicates)));
	}

	public Predicate not(Predicate pred) {
		return wrap(B.rxC((L) pred.delegate));
	}

	public Predicate notNull(String key) {
		return wrap(B.NxC(key));
	}

	public Predicate isTrue(String what) {
		return wrap(B.DxC(what));
	}

	public Predicate equals(String key, String value) {
		return wrap(B.VJB(key, value));
	}

	public Predicate inList(String key, Object membersArray) {
		return wrap(B.hxC(key, CatalogAdapter573
                .asUUIDArray((Object[]) membersArray)));
	}

	public Predicate greater(String key, long value, boolean inclusive) {
		return wrap(B.cvC(key, value, inclusive));
	}

	public Predicate startsWith(String key, String value) {
		return wrap(B.wwC(key, value));
	}

}
