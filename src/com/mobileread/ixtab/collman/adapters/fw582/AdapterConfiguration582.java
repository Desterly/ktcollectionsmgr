package com.mobileread.ixtab.collman.adapters.fw582;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;
import com.mobileread.ixtab.collman.adapters.SearchHandler;

public class AdapterConfiguration582 extends AdapterConfiguration {

	public AdapterConfiguration582() {

	};

	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter582();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter582();
	}

	public CatalogService getCatalogService() {
		return new CatalogService582();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager582(context);
	}

	public SearchHandler getSearchHandler() {
		return new SearchHandler582();
	}

}
