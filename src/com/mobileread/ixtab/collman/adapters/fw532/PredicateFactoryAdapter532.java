package com.mobileread.ixtab.collman.adapters.fw532;

import com.amazon.kindle.content.catalog.b;
import com.amazon.kindle.content.catalog.g;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class PredicateFactoryAdapter532 implements PredicateFactoryAdapter {
	
	private Predicate wrap(Object wrapped) {
		return new Predicate(wrapped);
	}

	private g[] explode(
			Predicate[] in) {
		if (in == null) {
			return null;
		}
		g[] out = new g[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (g) in[i].delegate;
		}
		return out;
	}

	public Predicate startsWith(String key, String value) {
		return wrap(b.AdB(key, value));
	}

	public Predicate and(Predicate[] predicates) {
		return wrap(b.vcB(explode(predicates)));
	}

	public Predicate isTrue(String what) {
		return wrap(b.kdB(what));
	}

	public Predicate not(Predicate pred) {
		return wrap(b.hAB((g) pred.delegate));
	}

	public Predicate equals(String key, String value) {
		return wrap(b.IvA(key, value));
	}

	public Predicate or(Predicate[] predicates) {
		return wrap(b.IbB(explode(predicates)));
	}

	public Predicate notNull(String key) {
		return wrap(b.GAB(key));
	}

	public Predicate inList(String key, Object membersArray) {
		return wrap(b.qcB(key, CatalogAdapter532.asUUIDArray((Object[]) membersArray)));
	}

	public Predicate greater(String key, long value, boolean inclusive) {
		return wrap(b.sbB(key, value, inclusive));
	}

}
