package com.mobileread.ixtab.collman.adapters.fw581;

import java.util.Date;

import com.amazon.ebook.util.a.B;
import com.amazon.ebook.util.text.d;
import com.amazon.kindle.content.catalog.f;
import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;
import com.amazon.kindle.content.catalog.CatalogItem;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;

public class CatalogAdapter581 extends CatalogAdapter {

	public void setMembers(MutableCollection mutable, Object[] uuids) {
		B[] ids = asUUIDArray(uuids);
		mutable.KN(ids);
	}

	protected static B[] asUUIDArray(Object[] in) {
		if (in == null) {
			return null;
		}
		B[] out = new B[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (B) in[i];
		}
		return out;
	}

	public void addMember(MutableCollection mutable, Object uuid) {
		mutable.gO((B) uuid);
	}

	public void setIsVisibleInRoot(MutableCollection mutable,
			boolean visible) {
		setIsVisibleInHome(mutable, visible);
	}

	public void setTitle(MutableCollection mutable, String title) {
		setTitle((MutableEntry)mutable, title);
	}

	public Object getUUID(CatalogEntry entry) {
		return entry.Ul();
	}

	public String getLocation(CatalogEntry entry) {
		return entry.nJ();
	}

	public String getTitle(CatalogEntry entry) {
		d[] titles = entry.Sl();
		return (titles == null || titles.length < 1) ? "" : titles[0].toString();
	}

	public String getCDEKey(CatalogItem item) {
		return item.uK();
	}

	public String getCDEType(CatalogItem item) {
		return item.Al();
	}

	public Date getLastAccessDate(CatalogEntryCollection collection) {
		return collection.kl();
	}

	public boolean isVisibleInHome(CatalogEntry entry) {
		return entry.Dl();
	}

	public void setIsVisibleInHome(MutableEntry mutable, boolean visible) {
		mutable.uO(visible);
	}

	public boolean hasCloudCollections() {
		return true;
	}

	public int countParents(CatalogEntry entry) {
		return getParents(entry).length;
	}

	public Object[] getParents(CatalogEntry entry) {
		return entry.Gl();
	}

	public String getFirstCreditAsJSON(CatalogEntry entry) {
		f[] credits = entry.zJ();
		if (credits == null || credits.length < 1) {
			return null;
		}
		return credits[0].toJSONString();
	}

	public Object[] getMembers(CatalogEntryCollection collection) {
		return collection.eo();
	}

	public int countMembers(CatalogEntryCollection collection) {
		return collection.wn();
	}

	public void setTitle(MutableEntry mutable, String title) {
		mutable.tn(new d[] {new d(title)});
	}

}
