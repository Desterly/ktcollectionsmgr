package com.mobileread.ixtab.collman.adapters.fw585;

import com.amazon.kindle.content.catalog.B;
import com.amazon.kindle.content.catalog.L;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class PredicateFactoryAdapter585 implements PredicateFactoryAdapter {

	private Predicate wrap(Object wrapped) {
		return new Predicate(wrapped);
	}

	private L[] explode(Predicate[] in) {
		if (in == null) {
			return null;
		}
		L[] out = new L[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (L) in[i].delegate;
		}
		return out;
	}

	public Predicate and(Predicate[] predicates) {
		return wrap(B.EWC(explode(predicates)));
	}

	public Predicate or(Predicate[] predicates) {
		return wrap(B.owC(explode(predicates)));
	}

	public Predicate not(Predicate pred) {
		return wrap(B.ivC((L) pred.delegate));
	}

	public Predicate notNull(String key) {
		return wrap(B.EVC(key));
	}

	public Predicate isTrue(String what) {
		return wrap(B.kvC(what));
	}

	public Predicate equals(String key, String value) {
		return wrap(B.mNB(key, value));
	}

	public Predicate inList(String key, Object membersArray) {
		return wrap(B.IUC(key, CatalogAdapter585
                .asUUIDArray((Object[]) membersArray)));
	}

	public Predicate greater(String key, long value, boolean inclusive) {
		return wrap(B.nUC(key, value, inclusive));
	}

	public Predicate startsWith(String key, String value) {
		return wrap(B.aVC(key, value));
	}

}
