package com.mobileread.ixtab.collman.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.I18n;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.Settings;

public class ToggleFilterLowerPanelOnlyAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	public ToggleFilterLowerPanelOnlyAction() {
		super(I18n.get().i18n(I18n.FILTER_LOWERONLY_KEY, I18n.FILTER_LOWERONLY_VALUE));
	}

	public void actionPerformed(ActionEvent e) {
		Event.post(Settings.get().isFilterLowerPanelOnly() ? Event.FILTER_LOWER_ONLY_OFF : Event.FILTER_LOWER_ONLY_ON, this, PanelPosition.BOTH);
	}

}
