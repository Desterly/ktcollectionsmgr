package com.mobileread.ixtab.collman.adapters;

import com.amazon.kindle.content.catalog.MutableEntry;

public abstract class CatalogTransaction {

	public abstract void deleteEntry(Object uuid);

	public abstract boolean commitSync();
	
	public abstract void addEntry(MutableEntry c);
	
	public abstract void updateEntry(MutableEntry entry);
}
