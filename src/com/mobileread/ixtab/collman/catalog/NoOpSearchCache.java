package com.mobileread.ixtab.collman.catalog;


public class NoOpSearchCache implements SearchCache {

	public void delete(Object uuid) {
	}

	public void set(Object uuid, String newTitle) {
	}

	public Object[] find(String search) {
		return null;
	}

}
