package com.mobileread.ixtab.collman.adapters.fw540;

import com.amazon.ebook.util.a.i;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;

public class CatalogTransaction540 extends CatalogTransaction {

	private final com.amazon.kindle.content.catalog.CatalogTransaction delegate;
	public CatalogTransaction540(
			com.amazon.kindle.content.catalog.CatalogTransaction delegate) {
		this.delegate = delegate;
	}

	public void deleteEntry(Object uuid) {
		delegate.NI((i) uuid);
	}

	public boolean commitSync() {
		return delegate.xh().iSB();
	}

	public void addEntry(MutableEntry c) {
		delegate.CI(c);
	}

	public void updateEntry(MutableEntry entry) {
		delegate.jj(entry);
	}

}
