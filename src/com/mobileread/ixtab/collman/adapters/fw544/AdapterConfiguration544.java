package com.mobileread.ixtab.collman.adapters.fw544;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;
import com.mobileread.ixtab.collman.adapters.SearchHandler;

public class AdapterConfiguration544 extends AdapterConfiguration {

	public AdapterConfiguration544() {

	};

	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter544();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter544();
	}

	public CatalogService getCatalogService() {
		return new CatalogService544();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager544(context);
	}

	public SearchHandler getSearchHandler() {
		return new SearchHandler544();
	}

}
