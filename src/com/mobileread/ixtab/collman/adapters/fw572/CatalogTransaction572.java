package com.mobileread.ixtab.collman.adapters.fw572;

import com.amazon.ebook.util.a.B;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;

public class CatalogTransaction572 extends CatalogTransaction {

	private final com.amazon.kindle.content.catalog.CatalogTransaction delegate;
	public CatalogTransaction572(
			com.amazon.kindle.content.catalog.CatalogTransaction delegate) {
		this.delegate = delegate;
	}

	public void deleteEntry(Object uuid) {
		delegate.oN((B) uuid);
	}

	public boolean commitSync() {
		return delegate.uM().hxC();
	}

	public void addEntry(MutableEntry c) {
		delegate.Cp(c);
	}

	public void updateEntry(MutableEntry entry) {
		delegate.rp(entry);
	}

}
