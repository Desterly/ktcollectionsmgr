package com.mobileread.ixtab.collman.adapters.fw587;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;
import com.mobileread.ixtab.collman.adapters.SearchHandler;

public class AdapterConfiguration587 extends AdapterConfiguration {

	public AdapterConfiguration587() {

	};

	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter587();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter587();
	}

	public CatalogService getCatalogService() {
		return new CatalogService587();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager587(context);
	}

	public SearchHandler getSearchHandler() {
		return new SearchHandler587();
	}

}
