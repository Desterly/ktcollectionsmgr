package com.mobileread.ixtab.collman.adapters.fw565;

import java.util.Date;

import com.amazon.ebook.util.a.B;
import com.amazon.ebook.util.text.d;
import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;
import com.amazon.kindle.content.catalog.CatalogItem;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.amazon.kindle.content.catalog.f;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;

public class CatalogAdapter565 extends CatalogAdapter {

	public void setMembers(MutableCollection mutable, Object[] uuids) {
		B[] ids = asUUIDArray(uuids);
		mutable.zo(ids);
	}

	protected static B[] asUUIDArray(Object[] in) {
		if (in == null) {
			return null;
		}
		B[] out = new B[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (B) in[i];
		}
		return out;
	}

	public void addMember(MutableCollection mutable, Object uuid) {
		mutable.Qp((B) uuid);
	}

	public void setIsVisibleInRoot(MutableCollection mutable,
			boolean visible) {
		setIsVisibleInHome(mutable, visible);
	}

	public void setTitle(MutableCollection mutable, String title) {
		setTitle((MutableEntry)mutable, title);
	}

	public Object getUUID(CatalogEntry entry) {
		return entry.JL();
	}

	public String getLocation(CatalogEntry entry) {
		return entry.Jl();
	}

	public String getTitle(CatalogEntry entry) {
		d[] titles = entry.EL();
		return (titles == null || titles.length < 1) ? "" : titles[0].toString();
	}

	public String getCDEKey(CatalogItem item) {
		return item.Ol();
	}

	public String getCDEType(CatalogItem item) {
		return item.dk();
	}

	public Date getLastAccessDate(CatalogEntryCollection collection) {
		return collection.Tm();
	}

	public boolean isVisibleInHome(CatalogEntry entry) {
		return entry.FL();
	}

	public void setIsVisibleInHome(MutableEntry mutable, boolean visible) {
		mutable.nN(visible);
	}

	public boolean hasCloudCollections() {
		return true;
	}

	public int countParents(CatalogEntry entry) {
		return getParents(entry).length;
	}

	public Object[] getParents(CatalogEntry entry) {
		return entry.OJ();
	}

	public String getFirstCreditAsJSON(CatalogEntry entry) {
		f[] credits = entry.Fl();
		if (credits == null || credits.length < 1) {
			return null;
		}
		return credits[0].toJSONString();
	}

	public Object[] getMembers(CatalogEntryCollection collection) {
		return collection.ak();
	}

	public int countMembers(CatalogEntryCollection collection) {
		return collection.Pk();
	}

	public void setTitle(MutableEntry mutable, String title) {
		mutable.Qn(new d[] {new d(title)});
	}

}
