package com.mobileread.ixtab.collman.adapters.fw572;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;
import com.mobileread.ixtab.collman.adapters.SearchHandler;

public class AdapterConfiguration572 extends AdapterConfiguration {

	public AdapterConfiguration572() {

	};

	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter572();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter572();
	}

	public CatalogService getCatalogService() {
		return new CatalogService572();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager572(context);
	}

	public SearchHandler getSearchHandler() {
		return new SearchHandler572();
	}

}
