package com.mobileread.ixtab.collman.adapters.fw58501;

import com.amazon.ebook.util.a.B;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;

public class CatalogTransaction58501 extends CatalogTransaction {

	private final com.amazon.kindle.content.catalog.CatalogTransaction delegate;
	public CatalogTransaction58501(
			com.amazon.kindle.content.catalog.CatalogTransaction delegate) {
		this.delegate = delegate;
	}

	public void deleteEntry(Object uuid) {
		delegate.SO((B) uuid);
	}

	public boolean commitSync() {
		return delegate.Sn().awC();
	}

	public void addEntry(MutableEntry c) {
		delegate.Sp(c);
	}

	public void updateEntry(MutableEntry entry) {
		delegate.iN(entry);
	}

}
