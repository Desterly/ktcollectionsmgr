package com.mobileread.ixtab.collman.adapters.fw587;

import com.amazon.kindle.content.catalog.B;
import com.amazon.kindle.content.catalog.L;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class PredicateFactoryAdapter587 implements PredicateFactoryAdapter {

	private Predicate wrap(Object wrapped) {
		return new Predicate(wrapped);
	}

	private L[] explode(Predicate[] in) {
		if (in == null) {
			return null;
		}
		L[] out = new L[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (L) in[i].delegate;
		}
		return out;
	}

	public Predicate and(Predicate[] predicates) {
		return wrap(B.bXC(explode(predicates)));
	}

	public Predicate or(Predicate[] predicates) {
		return wrap(B.YvC(explode(predicates)));
	}

	public Predicate not(Predicate pred) {
		return wrap(B.LUC((L) pred.delegate));
	}

	public Predicate notNull(String key) {
		return wrap(B.LwC(key));
	}

	public Predicate isTrue(String what) {
		return wrap(B.KWC(what));
	}

	public Predicate equals(String key, String value) {
		return wrap(B.lNB(key, value));
	}

	public Predicate inList(String key, Object membersArray) {
		return wrap(B.hwC(key, CatalogAdapter587
                .asUUIDArray((Object[]) membersArray)));
	}

	public Predicate greater(String key, long value, boolean inclusive) {
		return wrap(B.evC(key, value, inclusive));
	}

	public Predicate startsWith(String key, String value) {
		return wrap(B.jvC(key, value));
	}

}
