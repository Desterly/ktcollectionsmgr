package com.mobileread.ixtab.collman.ui.menu.display;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.Settings;
import com.mobileread.ixtab.collman.actions.ToggleShowCollectionsOnlyAction;

public class ShowCollectionsOnlyButton extends JCheckBox implements ActionListener {
	private static final long serialVersionUID = 1L;

	public ShowCollectionsOnlyButton() {
		super(new ToggleShowCollectionsOnlyAction());
		setSelected(Settings.get().isShowCollectionsOnly());
		Event.addListener(this);
	}
	
	public void actionPerformed(ActionEvent event) {
		Event e = (Event) event;
		if (e.getID() == Event.DISPLAY_ALL) {
			setSelected(false);
		} else if (e.getID() == Event.DISPLAY_COLLECTIONS_ONLY) {
			setSelected(true);
		}
	}
}

